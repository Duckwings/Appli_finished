Application des elèves de PoP School pour mettre en ligne leurs veilles. Inscrivez-vous si vous faite partie de la team qui reste a l'actualité et partager un max les dernières infos !

Comment installer l'application :

 - Renseigner votre identifiant, votre mot de passe, et le nom de votre base de données dans le fichier database.php.sample que vous renommerez database.php

 - Pour importer votre base de données, il vous suffira d'aller sur votre PHP My Admin, de créer une nouvelle BDD, puis vous n'aurez plus qu'à importer le fichier veilleApp.sql


Profitez pleinement de l'application !



