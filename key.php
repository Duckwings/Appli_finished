<?php
include ('core/session.php');
include ('core/database.php');
include ('core/loged.php');
 ?>
<!doctype html>
<html>
<head>
  <title>Catégories</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="template/style.css">
  <style>

  </style>
</head>
<body>
  <?php include('template/header.php'); ?>
  <div class="container-fluid">
    <div class="col-xs-8 col-xs-offset-2">
    <div class="row">
      <h3>Catégories  </h3>
</div>
      <div class="row categorie">
        <div id="form" class="col-xs-8">
          <form class="key" action="key.php" method="post">
            <select name="keyword" tabindex="1" class="form-control">
              <option value="Web">Web</option>
              <option value="Objet connecté">Objet connecté</option>
              <option value="Logiciel">Logiciel</option>
              <option value="Hack">Hack</option>
              <option value="Robotique">Robotique</option>
              <option value="Autre">Autre</option>
            </select>
          </form>
        </div>
        <div class=" col-xs-2">
          <input type="submit" tabindex="2" name="submit" class="btn btn-info" value="Sélectionner">
        </div>
        <div class="col-xs-2 " id="cancel">
          <input type="submit" tabindex="3" name="cancel" class="btn btn-info" value="Reset">
        </div>
    </div>
  </div>
</div>
    <div class="row ">
      <div class=" col-xs-8 col-xs-offset-2">
        <?php
        if(isset($_POST['submit'])) {
          $keyword=$_POST['keyword'];
          $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille WHERE keyword='$keyword'";
          $result = mysqli_query($handle,$query);
        } elseif(isset($_POST['cancel'])) {
          header('Location:key.php');
        } else {
          $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille";
          $result = mysqli_query($handle,$query);
        }
        if($handle->affected_rows > 0) {
          while($line=mysqli_fetch_array($result)) {
            $i--;

            echo "\t\t<div id='veille_membre'>\n";
            echo "\t\t\t<div class='row'>\n";
            echo "\t\t\t\t<div class='col-xs-2'>\n";
            echo "\t\t\t\t\t<p class='dark'>" .$line['date_formatee']."</p>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-7'>\n";
            echo "\t\t\t\t\t<div class='subject'>\n";
            $title = $line['subject'];
            if(strlen($title) > 60){
              $title = substr($title, 0, 60) ."...";
            }
            echo "\t\t\t\t\t\t<p class='titre'> <a href='veille.php?id=".$line['id']."'><img class='sujet_img' src='img/sujet.png'>   " . $title." </a></p>\n";
            echo "\t\t\t\t\t\t<p> <img class='key_img' src='img/key.png'> ".$line['keyword']."</p>\n";
            echo "\t\t\t\t\t</div>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-2 col-xs-offset-1'>\n";
            echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='img/go.png'></a>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t</div>\n";
            echo "\t\t</div>\n";
          }
        } else {
          echo "\t\t\t<p>Aucune veille n'a été postée pour le moment...</p>\n";
        }
        ?>
      </div>
    </div>
  </div>
<?php include ('template/footer.php'); ?>
