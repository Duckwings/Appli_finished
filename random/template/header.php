<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- PROBLEME AVEC LE LOGO	-->
    <a class="navbar-brand" href="index.php"><span class="school">POP</span>School</a>
     </div>
     <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
       <ul class="nav navbar-nav navbar-left">
         <li <?php if (basename($_SERVER['PHP_SELF']) == 'chat.php') echo 'class="active"'; ?>><a href="chat.php"><span class="menu">Chat</span></a></li>
         <li <?php if (basename($_SERVER['PHP_SELF']) == 'randomizer.php') echo 'class="active"'; ?>><a href="randomizer.php"><span class="menu">Random</span></a></li>
         <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">FILTRE<span class="caret"></span></a>
           <ul class="dropdown-menu">
             <li><a href="name.php">Popschoolers</a></li>
             <li><a href="#">Date</a></li>
             <li><a href="key.php">Catégorie</a></li>
           </ul>
         </li>
       </ul>
       <ul class="nav navbar-nav navbar-right">
<?php
  if ($logged==1) {
    echo "\t\t\t<li" . (basename($_SERVER['PHP_SELF']) == 'membre.php' ? ' class="active" ' : '') . "><a href='membre.php'>Espace membre</a></li>\n";
    echo "\t\t\t<li" . (basename($_SERVER['PHP_SELF']) == 'logout.php' ? ' class="active" ' : '') . "><a href='core/logout.php'>Déconnexion</a></li>\n";
  }
  elseif ($logged==2) {
    echo "\t\t\t<li" . (basename($_SERVER['PHP_SELF']) == 'login.php' ? ' class="active" ' : '') . "><a href='login.php'>Connexion</a></li>\n";
    echo "\t\t\t<li" . (basename($_SERVER['PHP_SELF']) == 'register.php' ? ' class="active" ' : '') . "><a href='register.php'>Inscription</a></li>\n";
  }
?>
       </ul>
     </div>
   </div>
 </nav>
