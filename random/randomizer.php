<?php
include ('core/session.php');
include ('core/database.php');
include ('core/loged.php');
 ?>

 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
  <title>Random JS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap.theme.min.css">
  <link rel="stylesheet" href="css/main.css">

  <script src="js/random.js" type="text/javascript"></script>

</head>

<body onload="ShowPicture()">

<?php include('template/header.php'); ?>




<div id="pic"></div>


<input type="button" value="RANDOM" onclick="GetValue();" />
<div id="message"></div>
<audio id="audio" src="snare.mp3"></audio>

<button type="button" onClick="window.location.reload();">Reload</button>

<?php include ('template/footer.php'); ?>
