<?php
include ('core/session.php');
include ('core/database.php');
include ('core/loged.php');
?>
<!doctype html>
<html>
<head>
	<title>Modifications</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="template/style.css">
</head>
<body>
<?php include('template/header.php'); ?>
<div class="container">
  <?php

  $id_veille=$_GET['id'];
  $query="SELECT * FROM veille WHERE id='$id_veille'";
  $result=mysqli_query($handle,$query);
  $line=mysqli_fetch_array($result);
  $subject=$line['subject'];
  $title=$line['title'];
  $keyword=$line['keyword'];
   function key_1($keyword) {
      if($keyword=="Web"){
         return "selected";
      }
   }
   function key_2($keyword) {
      if($keyword=="Objets connecté"){
         return "selected";
      }
   }
   function key_3($keyword) {
      if($keyword=="Logiciel"){
         return "selected";
      }
   }
   function key_4($keyword) {
      if($keyword=="Hacks"){
         return "selected";
      }
   }
   function key_5($keyword) {
      if($keyword=="Robotique"){
         return "selected";
      }
   }
   function key_6($keyword) {
      if($keyword=="Autres"){
         return "selected";
      }
   }

  if ($id==$line['id_user']) {
     if (isset($_POST["submit"])) {
         $id=$_GET['id'];
         $subject=strip_tags($_POST["subject"]);
         $title=strip_tags($_POST["title"]);
         if (isset($_POST['keyword'])) {
            $keyword=$_POST["keyword"];
         } else {
            $keyword=$line['keyword'];
         }

        if ($subject&&$title&&$keyword) {
            if (isset($_FILES['content']) && $_FILES['content']['error'] == 0) {
            if ($_FILES['content']['size'] <= 10000000) {
               $info= pathinfo($_FILES['content']['name']);
               $extension_info=$info['extension'];
               $extensions=array('jpg', 'jpeg', 'png', 'odt', 'txt', 'md', 'pdf', 'html', 'mp4');
               if (in_array($extension_info, $extensions)) {
                  $content=basename($_FILES['content']['tmp_name']).".".$extension_info;
                  move_uploaded_file($_FILES['content']['tmp_name'], 'uploads/'.$content);
                  $query="UPDATE veille SET subject=\"$subject\", title=\"$title\", keyword='$keyword' WHERE `id`='$id'";
                  $result = mysqli_query($handle,$query) or die(error2);

									$description=$_POST['description'];
								$sql=	"INSERT INTO contents (id_veille,nom,description) VALUES ($id_veille,$content,$description) WHERE id_veille='$id';
  										ON DUPLICATE KEY UPDATE  ";
                  //$sql="INSERT INTO contents SET nom='$content', description='$description', type='$extension_info' WHERE id_veille='$id'";
                  $req=mysqli_query($handle,$sql) or die(error);

                  if($handle -> affected_rows > 0) {
                     header('Location:membre.php');
                  } else {
                     echo $keyword;
										 echo "<br>";
										 echo $query;
										 echo "<br>";
										 echo $sql;
                     echo "<br>";
                     echo $_FILES['content']['name']."<br>".$_FILES['content']['error']."<br>".$_FILES['content']['tmp_name']."<br>".$_FILES['content']['size'];
                     echo "<p class='error'>*  Une erreur est survenue, veuillez recommencer...</p>";
                  }
               } else {
                  echo "<p class='error'>* Format de fichier non pris en charge</p>";
                  }
            } else {
               echo "<p class='error'>* La taille du fichier est trop importante</p>";
               }
         } else {
            $query="UPDATE veille SET subject='$subject', title='$title', keyword='$keyword' WHERE `id`='$id'";
            $result= mysqli_query($handle,$query);
            if($handle->affected_rows > 0) {
               header('Location:membre.php');
            } else {
               echo "<p class='error'>*  Une erreur est survenue, veuillez recommencer...</p>";
               }
            }
        } else {
            echo "<p class='error'>* Veuillez renseigner tout les champs </p>";
        }
      }
   } else {
      header('Location:index.php');
   }

?>

<img class="modif" src="modif.png" alt="" />
		 <h3>Vous pouvez modifier votre veille</h3>

     <form method="POST" action="update_veille.php?id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data">
		   <div class="form-group">
            <label for="subject">Modifier le sujet </label>
			   <input type="text" class="form-control" tabindex="1" name="subject" placeholder="Sujet..." value="<?php echo $subject ?>">
		   </div>
		   <div class="form-group">
            <label for="title">Modifier le titre </label>
			   <input type="text" class="form-control" tabindex="2" name="title" placeholder="Titre..." value="<?php echo $title ?>">
		   </div>
		   <div class="form-group">
            <label>Catégories</label>
		         <select name="keyword" tabindex="3" class="form-control">
			         <option value="Web" <?php echo key_1($keyword); ?>>Web</option>
			         <option value="Objets connecté" <?php echo key_2($keyword); ?>>Objets connecté</option>
			         <option value="Logiciel" <?php echo key_3($keyword); ?>>Logiciel</option>
			         <option value="Hacks" <?php echo key_4($keyword); ?>>Hacks</option>
			         <option value="Robotique" <?php echo key_5($keyword); ?>>Robotique</option>
			         <option value="Autres" <?php echo key_6($keyword); ?>>Autres</option>
		         </select>
		   </div>
		   <div class="form-group">
				 <label for="content">Contenu</label>
            <input class="champ" tabindex="4" type="file" name="content"><br>
				<label for="description">Description</label>
				<textarea type='text' class='form-control' name="description" value="<?php echo $description ?>" ></textarea>

		   </div>
		   <button type="submit" tabindex="5" name ="submit" class="btn btn-info">Enregistrer les modifications</button>
	   </form>
	</div>

<?php include ('template/footer.php'); ?>
